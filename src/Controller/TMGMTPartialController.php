<?php

namespace Drupal\tmgmt_partial\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Language\LanguageInterface;
use Drupal\tmgmt\Entity\Job;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * {@inheritDoc}
 */
class TMGMTPartialController extends ControllerBase {

  /**
   * Controller for the cart items review route.
   *
   * @param string $tjid
   *   The ID of a mock TMGMT job containing items for review.
   * @param string $languages
   *   A base64-encoded json representing an array like ['de', 'es', 'ja'].
   */
  public function cartItemsReview($tjid, $languages) {
    $job = Job::load($tjid);
    // Retrieve info on target languages from the URL param.
    try {
      $languages = json_decode(base64_decode($languages), TRUE, 2, JSON_THROW_ON_ERROR);
      // Sanitize the values by comparing them against the list of possible
      // languages.
      $possible = array_keys($this->languageManager()->getLanguages());
      $languages = array_intersect($languages, $possible);
    }
    catch (\Exception $e) {
      // If the URL param was not correctly encoded, it's fine: we'll present
      // the languages <select> without any values pre-selected and let the
      // user choose their target languages again.
      $languages = [];
    }

    $languages = array_combine($languages, $languages);
    if ($job && $job->getTargetLangcode() === LanguageInterface::LANGCODE_NOT_APPLICABLE) {
      $build = [];
      $build['help'] = [
        '#markup' => '<p>' . $this->t('Select items to be translated.') . '</p>',
      ];
      $build['form'] = $this->formBuilder()->getForm('\Drupal\tmgmt_partial\Form\CartItemsReview', $job, $languages);
      return $build;
    }
    throw new NotFoundHttpException();
  }

}
