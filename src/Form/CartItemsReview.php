<?php

namespace Drupal\tmgmt_partial\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\Element;
use Drupal\tmgmt\Entity\Job;
use Drupal\tmgmt\Entity\JobItem;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * {@inheritDoc}
 */
class CartItemsReview extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tmgmt_partial_cart_items_review';
  }

  /**
   * Form builder.
   *
   * This form expects to be passed a mock TMGMT job with job items attached.
   * It presents these to the user and lets them select which data items should
   * actually be translated.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // The job passed here should be a temporary "mock" job, created only for
    // the purpose of this form, and assigned the "zxx" language code.
    $job = $form_state->getBuildInfo()['args'][0];
    $target_langcode = $job->getTargetLangcode();
    if (!($job instanceof Job) || ($target_langcode !== LanguageInterface::LANGCODE_NOT_APPLICABLE)) {
      throw new NotFoundHttpException();
    }

    // Data on which languages should be targeted can be passed in from the
    // cart.
    $target_languages = $form_state->getBuildInfo()['args'][1] ?? [];

    $form['job'] = [
      '#type' => 'value',
      '#value' => $job->id(),
    ];
    $data = $job->getData();

    $form['job_items'] = [
      '#type' => 'value',
      '#value' => array_keys($data),
    ];

    // The form presenting all data from all job items is loosely based on
    // the job item review form at \Drupal\tmgmt\Form\JobItemForm.
    foreach (Element::children($data) as $key) {
      $review_element = $this->reviewFormElement($form_state, \Drupal::service('tmgmt.data')->flatten($data[$key], $key), $key);
      if ($review_element) {
        $form[$key] = $review_element;
      }
    }

    $form['target_language'] = [
      '#type' => 'select',
      '#title' => $this->t('Request translation into language/s'),
      '#multiple' => TRUE,
      '#default_value' => $target_languages,
      '#options' => tmgmt_available_languages(),
      '#description' => $this->t('If the source language will be the same as the target language the item will be ignored.'),
    ];

    $form['request_translation'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Request translation of selected items'),
      '#submit' => ['::submitForm'],
      '#validate' => ['::validateSubmit'],
    ];

    $form['cancel'] = [
      '#type' => 'submit',
      '#button_type' => 'danger',
      '#value' => $this->t('Cancel'),
      '#submit' => ['::submitCancel'],
    ];

    return $form;
  }

  /**
   * Mostly copied from \Drupal\tmgmt\Form\JobItemForm::reviewFormElement().
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $data
   *   Flattened array of translation data items.
   * @param string $parent_key
   *   The key for $data.
   *
   * @return array|mixed|string|null
   *   Render array with the form element, or NULL if the text is not set.
   */
  protected function reviewFormElement(FormStateInterface $form_state, array $data, $parent_key) {
    $review_element = NULL;

    foreach (Element::children($data) as $key) {
      $data_item = $data[$key];
      if (isset($data_item['#text']) && \Drupal::service('tmgmt.data')->filterData($data_item)) {
        // The char sequence '][' confuses the form API so we need to replace
        // it when using it for the form field name.
        $field_name = str_replace('][', '|', $key);

        // Ensure that the review form structure is initialized.
        $review_element['#theme'] = 'tmgmt_partial_cart_items_review_form';
        $review_element['#ajaxid'] = tmgmt_review_form_element_ajaxid($parent_key);
        $review_element['#top_label'] = array_shift($data_item['#parent_label']);
        $leave_label = array_pop($data_item['#parent_label']);

        // Data items are grouped based on their key hierarchy, calculate the
        // group key and ensure that the group is initialized.
        $group_name = substr($field_name, 0, strrpos($field_name, '|'));
        if (empty($group_name)) {
          $group_name = '_none';
        }
        if (!isset($review_element[$group_name])) {
          $review_element[$group_name] = [
            '#group_label' => $data_item['#parent_label'],
          ];
        }

        // Initialize the form element for the given data item and make it
        // available as $element.
        $review_element[$group_name][$field_name] = [
          '#tree' => TRUE,
        ];
        $item_element = &$review_element[$group_name][$field_name];

        $item_element['label']['#markup'] = $leave_label;
        $item_element['actions'] = [
          '#type' => 'container',
          '#attributes' => ['class' => 'form-checkboxes'],
        ];
        $item_element['actions']['translate'] = [
          '#type' => 'checkbox',
          '#default_value' => FALSE,
        ];

        // Manage the height of the textareas, depending on the length of the
        // description. The minimum number of rows is 3 and the maximum is 15.
        $rows = ceil(strlen($data_item['#text']) / 100);
        $rows = min($rows, 15);
        $rows = max($rows, 3);

        $source_text = $data_item['#text'];
        $item_element['source'] = [
          '#type' => 'textarea',
          '#value' => $source_text,
          '#disabled' => TRUE,
          '#rows' => $rows,
        ];

      }
    }
    return $review_element;
  }

  /**
   * Make sure that there's at least one data item checked.
   */
  public function validateSubmit(array &$form, FormStateInterface $form_state) {
    $valid = FALSE;
    foreach ($form_state->getValues() as $key => $value) {
      // Ignore values that don't belong to a data item
      // (such as "target_language").
      if (is_array($value) && isset($value['actions'])) {
        // Ignore any item without a translate checkbox.
        if (!isset($value['actions']['translate'])) {
          continue;
        }
        // Make sure at least one translate checkbox is checked.
        if ((bool) $value['actions']['translate'] === TRUE) {
          $valid = TRUE;
        }
      }
    }
    if (!$valid) {
      $form_state->setError($form, $this->t('You must choose at least one item to be translated.'));
    }

    $target_languages = array_filter($form_state->getValue('target_language'));
    if (count($target_languages) < 1) {
      $form_state->setError($form, $this->t('You must choose at least one target language.'));
    }
  }

  /**
   * Main submit handler for the form.
   *
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $mock_job = $this->editMockJob($form_state);
    $this->submitMultipleJobs($mock_job, $form_state);
  }

  /**
   * Applies changes from the form to the mock tmgmt job.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface|\Drupal\tmgmt\Entity\Job|null
   *   Return value.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function editMockJob(FormStateInterface $form_state) {
    $job_id = $form_state->getValue('job');
    $job_item_ids = $form_state->getValue('job_items');

    $mock_job = Job::load($job_id);
    $job_items = JobItem::loadMultiple($job_item_ids);
    $job_items_to_save = [];

    // Save all the checkbox data.
    foreach ($form_state->getValues() as $key => $value) {
      // Ignore values that don't belong to a data item
      // (such as "target_language").
      if (is_array($value) && isset($value['actions'])) {
        // Ignore any item without a translate checkbox.
        if (!isset($value['actions']['translate'])) {
          continue;
        }
        // Ignore any item where the translate checkbox is checked.
        if ((bool) $value['actions']['translate'] === TRUE) {
          continue;
        }
        // For items where the translate checkbox is unchecked:
        // Find the piece of data in the job item, and set it to not translate.
        $key = explode('|', $key);
        $tjiid = array_shift($key);
        $job_item = &$job_items[$tjiid];
        $data = $job_item->getData($key);
        $data['#translate'] = FALSE;
        $job_item->updateData($key, $data);
        $job_items_to_save[$tjiid] = $tjiid;
        unset($job_item);
      }
    }
    // Save the changes to the job items, if there were any.
    foreach (array_keys($job_items_to_save) as $tjiid) {
      $job_items[$tjiid]->save();
    }

    return $mock_job;
  }

  /**
   * Submit one job per target language.
   *
   * Using the data from the mock job, assemble one real job per target
   * language, with clean copies of the job items, and go back to the regular
   * tmgmt multiple job submission flow.
   *
   * @param \Drupal\tmgmt\Entity\Job $mock_job
   *   Our customized job.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function submitMultipleJobs(Job $mock_job, FormStateInterface $form_state) {
    $jobs = [];
    $remove_job_item_ids = [];

    $source_language = $mock_job->getSourceLangcode();
    $target_languages = $form_state->getValue('target_language');
    foreach ($target_languages as $target_language) {
      // If the user accidentally chose the job's source language as one of
      // their target languages, ignore it.
      if ($source_language === $target_language) {
        continue;
      }

      $job_items = $mock_job->getItems();

      $job = tmgmt_job_create($source_language, $target_language, $this->currentUser()->id());
      $job_empty = TRUE;
      /** @var \Drupal\tmgmt\JobItemInterface $job_item */
      foreach ($job_items as $job_item) {
        try {
          // We can't add the same job item to multiple jobs, so we have to
          // re-create an equivalent item for each new job, and later delete
          // the original one.
          $old_data = $job_item->getData();
          $new_item = $job->addItem($job_item->getPlugin(), $job_item->getItemType(), $job_item->getItemId());
          foreach ($old_data as $key => $value) {
            if (!is_array($value)) {
              continue;
            }
            $new_item->updateData($key, $value, TRUE);
          }
          $new_item->save();
          $remove_job_item_ids[$job_item->id()] = $job_item->id();
          $job_empty = FALSE;
        }
        catch (\Exception $e) {
          $this->messenger()->addStatus($e->getMessage(), 'error');
        }
      }

      if (!$job_empty) {
        $jobs[] = $job;
      }
    }

    // Redirect to the submission process for the new jobs.
    if ($jobs) {
      // Delete the original job items.
      if ($remove_job_item_ids) {
        $storage = \Drupal::entityTypeManager()->getStorage('tmgmt_job_item');
        if ($delete_items = $storage->loadMultiple($remove_job_item_ids)) {
          $storage->delete($delete_items);
        }
      }
      // Delete the mock job.
      $storage = \Drupal::entityTypeManager()->getStorage('tmgmt_job');
      $storage->delete([$mock_job]);

      $form_state->setRedirect('tmgmt.admin_tmgmt');
      \Drupal::service('tmgmt.job_checkout_manager')->checkoutAndRedirect($form_state, $jobs);
    }
    else {
      $this->messenger()
        ->addError($this->t('From the selection you made it was not possible to create any translation job.'));
    }
  }

  /**
   * Submit handler for the cancel button.
   */
  public function submitCancel(array $form, FormStateInterface $form_state) {
    $mock_job_id = $form_state->getValue('job');
    $mock_job = Job::load($mock_job_id);
    $items = $mock_job->getItems();
    $storage = \Drupal::entityTypeManager()->getStorage('tmgmt_job_item');
    if (count($items)) {
      $storage->delete($items);
    }
    $storage = \Drupal::entityTypeManager()->getStorage('tmgmt_job');
    $storage->delete([$mock_job]);

    $form_state->setRedirect('tmgmt.admin_tmgmt');
    $this->messenger()->addStatus($this->t('Partial translation request was canceled.'));
  }

}
